<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Messeges;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Messege controller.
 *
 * @Route("messeges")
 */
class MessegesController extends Controller
{
    /**
     * Lists all messege entities.
     *
     * @Route("/", name="messeges_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $messeges = $em->getRepository('AppBundle:Messeges')->findAll();

        return $this->render('messeges/index.html.twig', array(
            'messeges' => $messeges,
        ));
    }

    /**
     * Creates a new messege entity.
     *
     * @Route("/new", name="messeges_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $messege = new Messeges();
        $form = $this->createForm('AppBundle\Form\MessegesType', $messege);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($messege);
            $em->flush();

            return $this->redirectToRoute('messeges_show', array('id' => $messege->getId()));
        }

        return $this->render('messeges/new.html.twig', array(
            'messege' => $messege,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a messege entity.
     *
     * @Route("/{id}", name="messeges_show")
     * @Method("GET")
     */
    public function showAction(Messeges $messege)
    {
        $deleteForm = $this->createDeleteForm($messege);

        return $this->render('messeges/show.html.twig', array(
            'messege' => $messege,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing messege entity.
     *
     * @Route("/{id}/edit", name="messeges_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Messeges $messege)
    {
        $deleteForm = $this->createDeleteForm($messege);
        $editForm = $this->createForm('AppBundle\Form\MessegesType', $messege);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('messeges_edit', array('id' => $messege->getId()));
        }

        return $this->render('messeges/edit.html.twig', array(
            'messege' => $messege,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a messege entity.
     *
     * @Route("/{id}", name="messeges_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Messeges $messege)
    {
        $form = $this->createDeleteForm($messege);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($messege);
            $em->flush();
        }

        return $this->redirectToRoute('messeges_index');
    }

    /**
     * Creates a form to delete a messege entity.
     *
     * @param Messeges $messege The messege entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Messeges $messege)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('messeges_delete', array('id' => $messege->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
